Zum Projektteam gehören Sarah Vrzina und Danial Sarfraz.
Die Aufgabenverteilung ist grundsätzlich für beide Mitglieder des Projektteams gleich verteilt.
Der dem Projekt zugrundeliegende Quellcode wird in Zusammenarbeit erstellt.
Abstufungen werden höchstens in den verschiedenen Funktionen vorgenommen, welche dann ggfs. aus dem Changelog ersichtlich werden.
