Beim Projekt "Nim-Spiel" handelt es sich um ein Strategiespiel für bis zu zwei Personen, welches im Board of Symbols gespielt wird.
Auf dem Spielfeld liegen in 4 Reihen eine jeweils inkrementierende Anzahl an Streichhölzern.
Jeder Spieler kann bei jedem Zug bis zu 3 Streichhölzer (aber mindestens 1) in einer Reihe entnehmen.
Wer das letzte Streichholz auf dem Spielfeld nimmt, hat das Spiel verloren.

Das Nim-Spiel dient zum einen der Beschäftigung und Bespaßung der Mitspieler.
Zum anderen kann es dazu verwendet werden, das vorausschauende Denken und Handeln sowohl zu fordern als auch zu fördern.

Die vermutlich größte technische Herausforderung vermuten wir in der Sicherstellung, dass nicht aus verschiedenen Reihen Streichhölzer genommen wurden.
Ein Lösungsansatz hierfür könnte darin bestehen, evtl. im Board of Symbols abzufragen, aus welcher Reihe das angeklickte Streichholz kommt.
Sollte das nicht möglich sein, kann man diese Funktionalität evtl. über Konsoleneingaben abbilden.