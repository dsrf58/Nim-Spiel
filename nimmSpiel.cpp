// nimmSpiel.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include <stdlib.h>

#define GROESSE_X 9
#define GROESSE_Y 4
#define MAX_HOELZER 3

int aktuelleReihe = -1;
int zugHoelzer = 0;
int streichhoelzer[] = { 31, 21, 23, 11, 13, 15, 1, 3, 7, 5 };

int streichhoelzer_laenge = sizeof(streichhoelzer) / sizeof(streichhoelzer[0]);
int array_streichhoelzer_laenge = streichhoelzer_laenge;
int selectedHoelzer[MAX_HOELZER] = { 0 };

int count = 0;
int used_streichhoelzer[10] = { -1 };
int used_streichhoelzer_laenge = 10;

char spieler1[20];
char spieler2[20];

int aktuellerSpieler = 1;
int gameHasEnded = 0;


int in_array (int needle, int haystack[], int size) {
	for (int i = 0; i < size; i++) {
		if (needle == haystack[i])
			return 1;
	}
	return 0;
}

int pruefeZug(int feld) {
	if (in_array(feld, streichhoelzer, array_streichhoelzer_laenge) == 1 && in_array(feld, used_streichhoelzer, used_streichhoelzer_laenge) == 0) {
		used_streichhoelzer[count] = feld;
		count++;
		
		return 0;
	}
	return 1;
}

void initSpiel() {
	loeschen();
	groesse(GROESSE_X, GROESSE_Y);
	formen("|");
	flaeche(WHITE);

	for (int i = 0; i < GROESSE_X * GROESSE_Y; i++) {
		if (in_array(i, streichhoelzer, array_streichhoelzer_laenge) == 0)
			farbe(i, WHITE);
		else
			farbe(i, BROWN);
	}

	form(34, "c");
	form(35, "c");
	farbe(34, RED);
	farbe(35, GREEN);
	

	printf("Geben Sie den Namen von Spieler 1 an: \t");
	scanf("%s", &spieler1[0]);
	printf("Geben Sie den Namen von Spieler 2 an: \t");
	scanf("%s", &spieler2[0]);
	printf("Viel Spass beim Nim-Spiel, %s und %s!\n\n", spieler1, spieler2);
	printf("Der rote Button setzt deinen Zug zurueck.\nDer gruene Button muss nach jedem Zug zur Bestaetigung betaetigt werden.\n");

}

void beendeZug() {	
	if (streichhoelzer_laenge > 0) {
		if (zugHoelzer > 0) {
			aktuelleReihe = -1;
			zugHoelzer = 0;
			printf("Der Zug von Spieler %d ist beendet.\n", aktuellerSpieler);
			aktuellerSpieler = (aktuellerSpieler == 1) ? 2 : 1;
			printf("Anzahl Streichhoelzer verbleibend: %d\n", streichhoelzer_laenge);
			for (int i = 0; i < MAX_HOELZER; i++)
				selectedHoelzer[i] = 0;	
		}
		else
			printf("Bitte mindestens ein Streichholz auswaehlen!\n");
	}
	else {
		gameHasEnded = 1;
		printf("%s hat leider verloren.\n", (aktuellerSpieler == 1) ? spieler1 : spieler2);
	}
}

void resetZug() {
	aktuelleReihe = -1;
	zugHoelzer = 0;
	printf("Der Zug wurde zurueckgesetzt.\n");
	for (int i = 0; i < MAX_HOELZER; i++) {
		if (selectedHoelzer[i] != 0) {
			farbe(selectedHoelzer[i], BROWN);
			streichhoelzer_laenge++;
			for (int j = 0; j < used_streichhoelzer_laenge; j++) {
				if (selectedHoelzer[i] == used_streichhoelzer[j]) {
					used_streichhoelzer[j] = -1;
					count--;
				}
			}
		}
	}
}



int _tmain(int argc, _TCHAR* argv[])
{

	int feld, x, y;
	initSpiel();

	while (gameHasEnded == 0) {
		char *a = abfragen();
		if (strlen(a) > 0) {
			if (a[0] == '#') {
				sscanf_s(a, "# %d %d %d", &feld, &x, &y);
				if (aktuelleReihe == -1 && feld != 34 && feld != 35) {
					aktuelleReihe = y;
					if (pruefeZug(feld) == 0) {
						//printf("Aktuelle Reihe ist %d\n", aktuelleReihe);
						selectedHoelzer[zugHoelzer] = feld;
						zugHoelzer++;
						streichhoelzer_laenge--;
						farbe(feld, WHITE);
					}
					else
						printf("Ungueltiger Zug. An dieser Stelle befindet sich kein Streichholz!\n");
				}
				else {
					if (y != aktuelleReihe && feld != 34 && feld != 35) {
						printf("Fehler: Hoelzer aus mehreren Reihen\n");
					}
					else {
						if (zugHoelzer < MAX_HOELZER && feld != 34 && feld != 35) {
							if (pruefeZug(feld) == 0) {
								selectedHoelzer[zugHoelzer] = feld;
								zugHoelzer++;
								streichhoelzer_laenge--;
								farbe(feld, WHITE);
							}
							else
								printf("Ungueltiger Zug. An dieser Stelle befindet sich kein Streichholzss!\n");
						}
						else if (feld != 34 && feld != 35)
							printf("Fehler: Mehr als %d Hoelzer!\n", MAX_HOELZER);
						else if (feld == 34)
							resetZug();
						else
							beendeZug();
					}
				}
			}
		}
		else
			Sleep(200);
	}

	system("pause");
	return 0;
}

